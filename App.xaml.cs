﻿namespace ZhApp;

public partial class App : Application
{
	public App()
	{
		InitializeComponent();

		MainPage = new AppShell();
	}

    protected async override void OnStart()
    {         
       await  Shell.Current.GoToAsync("//LoginPage");
    }

}
